import ng from 'angular';
import uiRouter from 'angular-ui-router';

let moduleName = 'app.pages.landing';
let page = ng.module(moduleName, [uiRouter]);

class LandingCtrl {
  constructor(Tests, $state) {
    this.model = {
      username: ''
    };
    this.Tests = Tests;
    this.$state = $state;
  }
  
  submit() {
    let username = this.model.username.trim();
    
    if (username) {
      this.Tests.create({ username: username })
        .then( testId => {
          this.$state.go('test', { id: testId });
        });
    }
  }
}

LandingCtrl.$inject = ['Tests', '$state'];

RouterConfig.$inject = ['$stateProvider'];

function RouterConfig($stateProvider) {
  $stateProvider
    .state('landing', {
      url: '/',
      template: require('./index.html'),
      title: 'Начните тест',
      controller: LandingCtrl,
      controllerAs: '$ctrl'
    });
}

page.config(RouterConfig);

export default moduleName;
