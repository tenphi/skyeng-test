import ng from 'angular';
import uiRouter from 'angular-ui-router';

let moduleName = 'app.pages.test';
let page = ng.module(moduleName, [uiRouter]);

class TestCtrl {
  constructor($state, $stateParams, Tests) {
    this.Tests = Tests;
    this.$state = $state;
    this.testId = $stateParams.id;
    
    this.updateState();
  }

  getTask = () => {
    return this.Tests.getTask(this.testId)
      .then( task => {
        this.task = task;

        return task;
      });
  }

  getStatus = () => {
    return this.Tests.getStatus(this.testId)
      .then( status => this.status = status );
  }

  next() {
    this.answer = '';
    this.secondAnswer = '';
    this.rightAnswer = '';
    this.updateState();
  }

  updateState() {
    this.getTask()
      .then( task => this.getStatus().then( () => {
        if (!task) {
          // setTimeout hack to fix $http response on records page
          // @TODO we need to understand the problem
          setTimeout( () => this.$state.go('records', { id: this.testId }) , 50);
        }
      }));
  }
  
  setAnswer(word) {
    if (this.answer) {
      if (this.answer !== word && !this.isButtonDisabled(word)) {
        this.secondAnswer = word;
      }
      return;
    }

    this.Tests.answer({ id: this.testId, answer: word })
      .then( rightAnswer => {
        this.answer = word;
        this.rightAnswer = rightAnswer;
        this.getStatus();
      });
  }

  getButtonType(word) {
    if (this.answer === word) {
      if (this.answer === this.rightAnswer) {
        return 'primary';
      } else {
        return 'danger';
      }
    } else if (this.secondAnswer === word) {
      if (this.secondAnswer === this.rightAnswer) {
        return 'primary';
      } else {
        return 'danger';
      }
    } else {
      return 'default';
    }
  }

  isButtonDisabled(word) {
    return this.answer && this.answer !== word && this.answer === this.rightAnswer;
  }
}

TestCtrl.$inject = ['$state', '$stateParams', 'Tests'];

RouterConfig.$inject = ['$stateProvider'];

function RouterConfig($stateProvider) {
  $stateProvider
    .state('test', {
      url: '/test/:id',
      template: require('./index.html'),
      title: 'Test',
      controller: TestCtrl,
      controllerAs: '$ctrl'
    });
}

page.config(RouterConfig);

export default moduleName;
