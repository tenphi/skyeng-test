import ng from 'angular';
import uiRouter from 'angular-ui-router';

let moduleName = 'app.pages.records';
let page = ng.module(moduleName, [uiRouter]);

class RecordsCtrl {
  constructor(Tests, $state, $stateParams) {
    this.testId = $stateParams.id;
    this.$state = $state;

    Tests.records()
      .then( records => this.records = records );
    Tests.getStatus(this.testId)
      .then( status => this.status = status );
  }

  again() {
    this.$state.go('landing');
  }
}

RecordsCtrl.$inject = ['Tests', '$state', '$stateParams'];

RouterConfig.$inject = ['$stateProvider'];

function RouterConfig($stateProvider) {
  $stateProvider
    .state('records', {
      url: '/records/:id',
      template: require('./index.html'),
      title: 'Таблица рекордов',
      controller: RecordsCtrl,
      controllerAs: '$ctrl'
    });
}

page.config(RouterConfig);

export default moduleName;
