import field from 'blocks/field';

class InputCtrl {
  constructor() {
    if (!this.type) {
      this.type = 'text';
    }
  }
}

let input = Object.assign({}, field, {
  template: ( require('./index.less'), require('./index.html') ),
  bindings: {
    id: '@',
    model: '=',
    label: '@',
    type: '@',
    disabled: '<'
  },
  controller: InputCtrl
});

export default input;