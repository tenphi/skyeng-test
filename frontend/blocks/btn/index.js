export default {
  template: ( require('./index.less'), require('./index.html') ),
  bindings: {
    type: '@',
    text: '@'
  },
  transclude: true
}
