require('./app.less');
require('./blocks/all.less');

import ng from 'angular';
import RouterConfig from './router-config';
import uiRouter from 'angular-ui-router';
import ngAnimate from 'angular-animate';
import ngBem from 'angular-bem';
import ApiGenerator from 'vendor/api-generator';

// pages
import LandingPage from 'pages/landing';
import TestPage from 'pages/test';
import RecordsPage from 'pages/records';

// components
import inputBlock from 'blocks/input';
import btnBlock from 'blocks/btn';

// services
import ConfigProvider from 'services/config';
import TestsService from 'services/tests';

ng.module('app', [
  uiRouter,
  ngAnimate,
  ApiGenerator,
  ngBem,
  LandingPage,
  TestPage,
  RecordsPage
])
  .service('Tests', TestsService)
  .provider('Config', ConfigProvider)
  .component('uiInput', inputBlock)
  .component('uiBtn', btnBlock)
  .config(['APIProvider', '$logProvider', 'ConfigProvider', function(APIProvider, $logProvider, ConfigProvider) {
    let Config = ConfigProvider.$get();

    APIProvider.debug(Config.env === 'development');
    APIProvider.config(Config.api);
    
    $logProvider.debugEnabled(Config.env === 'development');
  }])

  .config(RouterConfig)

  .run(['$rootScope', 'Config', function($rootScope, Config) {
    $rootScope.$config = Config;
  }]);
