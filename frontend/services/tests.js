Tests.$inject = ['API'];

function Tests(API) {
  return API.Tests;
}

export default Tests;
