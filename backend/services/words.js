import Promise from 'bluebird';
import Logger from '../logger';

let log = new Logger('words-service');

export default {
  init({ config, Word }) {
    let words = config.words;
    let engWords = Object.keys(config.words);
    
    return Promise.all(engWords.map( async engWord => {
      let word = await Word.findOne({ eng: engWord });
      
      if (!word) {
        word = new Word({ eng: engWord, rus: words[engWord] });
        log.info(`add new word '${engWord}' <-> '${words[engWord]}'`);
        
        word.save();
      }
    }));
  }
};