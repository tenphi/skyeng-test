import _ from 'lodash';
import Logger from '../logger';

let log = new Logger('tests-service');

export default {
  async addTask(test) {
    let { Word } = this;
    let oldWordsIds = test.tasks.map( task => task.word );
    let wordsIds = (await Word.find({ _id: { $nin: oldWordsIds } })).map( word => word._id );

    if (!wordsIds.length) {
      return false;
    }

    let newWordId = _.sample(wordsIds);
    let option = Math.random() < .5 ? 'eng' : 'rus';

    // find 3 random answers
    let answerWords = await Word.find({ _id: { $ne: newWordId } });
    let answerWordsIds = _.sample(answerWords.map( word => word._id ), 3);

    let task = {
      word: newWordId,
      answers: answerWordsIds,
      option
    };

    test.tasks.push(task);
    test.markModified('tasks');
    test.save();

    return task;
  },

  async prepareTask(task) {
    let { Word } = this;
    let word = await Word.findOne({ _id: task.word });
    let answerWords = await Word.find({ _id: { $in: task.answers }});
    let answer;
    
    if (task.answer) {
      answer = await Word.findOne({ _id: task.answer });
    }

    if (task.option === 'eng') {
      return {
        word: word.eng,
        answers: _.shuffle(answerWords.map(word => word.rus).concat([word.rus])),
        answer: answer && answer.rus
      };
    } else {
      return {
        word: word.rus,
        answers: _.shuffle(answerWords.map(word => word.eng).concat([word.eng])),
        answer: answer && answer.eng
      };
    }
  },
  
  async answer(id, answer) {
    let { Test, Word } = this;
    let test = await Test.findOne({ _id: id });

    if (!test.tasks.length) {
      throw 'no tasks';
    }

    let task = _.last(test.tasks);

    if (task.answer) {
      throw 'already answered';
    }

    let word = await Word.findOne({ _id: task.word });
    let answerWord = await Word.findOne({ $or: [ { eng: answer }, { rus: answer } ] });

    if (!~task.answers.concat([task.word]).map( id => id.toString() ).indexOf(answerWord._id.toString() )) {
      throw 'it\'s not an answer';
    }

    task.answer = answerWord._id;

    if (task.word.equals(answerWord._id)) {
      task.result = true;
    } else {
      task.result = false;
    }

    test.markModified('tasks');
    test.save();

    return task.option === 'eng' ? word.rus : word.eng;
  },

  async updateScore(test) {
    let tasks = test.tasks;

    test.score = tasks.filter( task => task.answer && task.word.equals(task.answer)).length;

    await test.save();

    return test.score;
  },

  async updateMistakes(test) {
    let tasks = test.tasks;

    test.mistakes = tasks.filter( task => task.answer && !task.word.equals(task.answer)).length;

    await test.save();

    return test.mistakes;
  },

  async complete(test) {
    if (test.completed) return test;

    await this.updateScore(test);
    await this.updateMistakes(test);

    test.completed = true;

    await test.save();

    return test;
  },

  async canAddTask(test) {
    let mistakes = await this.updateMistakes(test);

    return !test.completed && mistakes < this.config.mistakesAllowed;
  }
};
