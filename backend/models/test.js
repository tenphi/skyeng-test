import db from '../db';
import timestamps from 'mongoose-timestamp';

let Schema = db.Schema;

let testSchema = new Schema({
  username: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 32
  },
  tasks: [{
    word: { 
      type: Schema.Types.ObjectId, 
      ref: 'Word', 
      required: true 
    },
    answers: [{
      type: Schema.Types.ObjectId,
      ref: 'Word'
    }],
    option: {
      type: String,
      required: true
    }, // 'eng' or 'rus'
    answer: {
      type: Schema.Types.ObjectId,
      ref: 'Word'
    },
    result: { // true - if correct, false - if not
      type: Boolean
    }
  }],
  completed: {
    type: Boolean,
    default: false
  },
  score: {
    type: Number,
    default: 0
  },
  mistakes: {
    type: Number,
    default: 0
  }
}, { strict: true });

testSchema.plugin(timestamps);

let Test = db.initialized ? db.model('Test', testSchema) : {};

export default Test;
