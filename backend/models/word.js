import db from '../db';
import timestamps from 'mongoose-timestamp';
import random from 'mongoose-random';

let Schema = db.Schema;

let wordSchema = new Schema({
  eng: {
    type: String,
    index: true
  },
  rus: {
    type: String,
    index: true
  }
}, { strict: true });

wordSchema.plugin(timestamps);
wordSchema.plugin(random, { path: 'r' });

let Word = db.initialized ? db.model('Word', wordSchema) : {};

export default Word;
