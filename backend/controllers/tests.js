import Controller from '../controller';
import Logger from '../logger';
import Tests from '../services/tests';
import _ from 'lodash';

let log = new Logger('tests-ctrl');

export default class TestsCtrl extends Controller {
  constructor(req, res) {
    super(req, res);
  }

  async create({ username }) {
    let { Test } = this;

    let test = new Test({ username });

    await test.save();
    
    return test._id;
  }

  async getStatus(id) {
    let { Test } = this;
    let test = await Test.findOne({ _id: id });

    return {
      tasks: test.tasks.length,
      score: await Tests.updateScore(test),
      completed: test.completed,
      mistakes: await Tests.updateMistakes(test)
    };
  }

  async getTask(id) {
    let { Test } = this;
    let test = await Test.findOne({ _id: id });

    if (!(await Tests.canAddTask(test))) {
      Tests.complete(test);
      return false;
    }

    // if there is no tasks or last task was answered
    if (!test.tasks.length || _.last(test.tasks).answer) {
      // create new task
      let task = await Tests.addTask(test);

      if (!task) {
        Tests.complete(test);
        return false;
      }

      return Tests.prepareTask(task);
    } else {
      // or return last task
      return Tests.prepareTask(_.last(test.tasks));
    }
  }

  async records() {
    return (await this.Test.find({ completed: true }).sort({ score: -1, mistakes: 1 }).limit(10))
      .map( record => {
        record = record.toObject();

        delete record.tasks;

        return record;
      });
  }

  async answer({ id, answer }) {
    return Tests.answer(id, answer);
  }
};
