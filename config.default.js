import env from './bundler/env';

export default class Config {
  constructor(mode, isPublic) {
    let ssl = env.get('ssl') || false;
    let host = env.get('host') || (mode === 'production' ? 'skyeng-test.com' : 'localhost');
    let port = env.get('port') || (mode === 'production' ? 8080 : 3000);

    this.env = mode;
    this.host = host;
    this.port = port;
    this.ssl = ssl;
    this.url = `http${(ssl ? 's' : '')}://${host}${(mode === 'production' ? '' : ':' + port)}/`;
    this.title = 'SkyEng';
    this.mistakesAllowed = 3;

    if (!isPublic) {
      this.paths = {
        backend: './backend',
        frontend: './frontend',
        universal: './universal',
        public: './public',
        assets: './public/assets'
      };

      this.db = {
        main: {
          host: 'localhost',
          port: 27017,
          name: 'skyeng'
        }
      };

      this.words = {
        "apple": "яблоко",
        "peach": "персик",
        "orange": "апельсин",
        "grape": "виноград",
        "lemon": "лимон",
        "pineapple": "ананас",
        "watermelon": "арбуз",
        "coconut": "кокос",
        "banana": "банан",
        "pomelo": "помело",
        "strawberry": "клубника",
        "raspberry": "малина",
        "melon": "дыня",
        "apricot": "абрикос",
        "mango": "манго",
        "plum": "слива",
        "pomegranate": "гранат",
        "cherry": "вишня"
      };
    }
  }
};
